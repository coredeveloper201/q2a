<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Image;
use App\User;
use Validator;
use App\Answer;
use App\Comment;
use App\Question;
use App\Category;
use App\QuestionVote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebsiteController extends Controller
{
    public function index(){
        return view('website.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeQuestion(Request $request)
    {
        // Validate form data
        $rules = array(
            'question' => 'required|string|max:255',
            'question_description' => 'required|string',
            'optional' => 'sometimes|string|max:255',
//            'user_id' => 'required|integer',
            'category_id' => 'required|integer',
            'tags' => 'required',
        );
        $validator = Validator::make ( $request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 5000,
                'error' => $validator->errors()
            ], 200);
        }

//         Create a model instance
        $question = new Question();
        $question->question             = $request->question;
        $question->question_description = $request->question_description;
        $question->optional             = $request->optional;
        $question->user_id              = Auth::user()->id;
        $question->category_id          = $request->category_id;
        $question->save();
        $question->tag($request->tags);
        return response()->json($question);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAnswer(Request $request)
    {
        // Validate form data
        $rules = array(
            'answer' => 'required|string',
            'user_id' => 'required|integer',
            'question_id' => 'required|integer',
        );
        $validator = Validator::make ( $request->all(), $rules);

        if ($validator->fails()){
            return response()->json(array('errors'=> $validator->getMessageBag()->toarray()));
        }

        // Create a model instance
        $answer = Answer::create($request->all());

        return response()->json($answer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeComment(Request $request)
    {
        // Validate form data
        $rules = array(
            'comment' => 'required|string',
            'user_id' => 'required|integer',
            'question_id' => 'required|integer',
        );
        $validator = Validator::make ( $request->all(), $rules);

        if ($validator->fails()){
            return response()->json(array('errors'=> $validator->getMessageBag()->toarray()));
        }

        // Create a model instance
        $comment = Comment::create($request->all());

        return response()->json($comment);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeVote(Request $request)
    {
        // Validate form data
        $rules = array(
            'vote_count' => 'sometimes|integer',
            'user_id' => 'required|integer',
            'question_id' => 'required|integer',
        );
        $validator = Validator::make ( $request->all(), $rules);

        if ($validator->fails()){
            return response()->json(array('errors'=> $validator->getMessageBag()->toarray()));
        }

        // Create a model instance
        $vote = QuestionVote::create($request->all());

        return response()->json($vote);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request, User $user)
    {
        // Validate form data
        $rules = array(
            'username' => 'required|string|max:255',
            'full_name' => 'sometimes|string|max:255',
            'location' => 'sometimes|string|max:255',
            'personal_website' => 'sometimes|string|max:255',
            'about' => 'sometimes|string|max:255',
            'facebook_profile_link' => 'sometimes|string|max:255',
            'twitter_profile_link' => 'sometimes|string|max:255',
            'linkedin_profile_link' => 'sometimes|string|max:255',
            'youtube_profile_link' => 'sometimes|string|max:255',
            'private_message' => 'sometimes|boolean',
            'wall_post' => 'sometimes|boolean',
            'mass_mailings' => 'sometimes|boolean',
            'avatar' => 'sometimes|image',
        );
        $validator = Validator::make ( $request->all(), $rules);

        if ($validator->fails()){
            return response()->json(array('errors'=> $validator->getMessageBag()->toarray()));
        }

        // Handle image upload
        if ($request->file('avatar')->isValid()) {
            //get filename with extension
            $filenamewithextension = $request->file('avatar')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('avatar')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('avatar')->storeAs('public/images/profile_images/thumbnail', $filenametostore);

            //Resize image here
            $thumbnailpath = public_path('storage/images/profile_images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(30, 30, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
        }

        // Find the user model & assign form value then save to database
        $user = User::find($user->id);
        $user->username = $request->username;
        $user->full_name = $request->full_name;
        $user->location = $request->location;
        $user->personal_website = $request->personal_website;
        $user->about = $request->about;
        $user->facebook_profile_link = $request->facebook_profile_link;
        $user->twitter_profile_link = $request->twitter_profile_link;
        $user->linkedin_profile_link = $request->linkedin_profile_link;
        $user->youtube_profile_link = $request->youtube_profile_link;
        $user->private_message = $request->private_message;
        $user->wall_post = $request->wall_post;
        $user->mass_mailings = $request->mass_mailings;
        $user->avatar = 'storage/images/profile_images/thumbnail/'.$filenametostore;
        $user->save();

        return response()->json(['status' => 2000, 'user' => $user]);
    }

    public function getCategories(){
        $categories = Category::where('status', 'active')->get();
        return response()->json($categories);
    }


    public function getQuestions(){
        $questions = DB::table('questions')
            ->join('categories', 'questions.category_id', '=', 'categories.id')
            ->get();
        return $questions;


    }

//    public function askQuestion(){
//        return view('website.ask_question');
//    }
//
//    public function userProfile(){
//        return view('website.user_profile');
//    }
//
//    public function userQuestions(){
//        return view('website.user_questions');
//    }
//
//    public function userAnswers(){
//        return view('website.user_answers');
//    }
//
//    public function userPoints(){
//        return view('website.user_points');
//    }

}
