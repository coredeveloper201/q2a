
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import router from './routes';
import TopHeader from './components/included/top_header';
import MyHeader  from './components/included/header';
import MyFooter  from './components/included/footer';

import Vue from 'vue'
import VuePageTitle from 'vue-page-title'

Vue.use(VuePageTitle, {
    prefix: 'Q2A | ',
    // suffix: ''
})

const app = new Vue({
    router,
    components :{
        'top-header':TopHeader,
        'my-header':MyHeader,
        'my-footer':MyFooter,
    }
}).$mount('#app')

