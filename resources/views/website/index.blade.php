@extends('website.layout')

@section('title')
    Home
@endsection


@section('content')
    <div id="app">
        <top-header></top-header>
        <my-header></my-header>
        <router-view></router-view>
        <my-footer></my-footer>
    </div>
@endsection